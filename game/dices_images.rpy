image dice_base = "images/dice-sheet.png"

# Dice Images
image dice1 = Crop((32,32,32,32), "dice_base")
image dice2 = Crop((128,32,32,32), "dice_base")
image dice3 = Crop((224,32,32,32), "dice_base")
image dice4 = Crop((320,32,32,32), "dice_base")
image dice5 = Crop((416,32,32,32), "dice_base")
image dice6 = Crop((512,32,32,32), "dice_base")
