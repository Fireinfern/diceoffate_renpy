## Dice screen #################################################################

default current_dice = 1

default is_rolling = False

default finish_rolling = False

default rolls_left = 2

# Python Block
init python:

    # Calback of timer function
    # initialy working with timer package but its not available in webgl build
    def timer_callback(trans, st, at):
        store.current_dice = renpy.random.randint(1, 6)
        store.is_rolling = False
        store.finish_rolling = True
        # Saves the last state of the interaction so it can be seen on the screen
        renpy.restart_interaction()

    # Function to be called when you click the roll dice button
    def roll_dice():
        store.is_rolling = True
        store.finish_rolling = False
        store.rolls_left -= 1

    # Setups the variables needed for the dice roll
    def initialize_roll():
        store.current_dice = 1
        store.is_rolling = False
        store.finish_rolling = False
        store.rolls_left = 2

    # Jump to a section depending of dice output
    def dice_result(sectionA, sectionB):
        if(store.current_dice > 4):
            renpy.jump(sectionA)
            return
        if (store.current_dice > 2):
            renpy.jump(sectionB)

# image dice roll with timer callback
image dice_roll:
    block:
        "dice1"
        pause 0.1
        "dice2"
        pause 0.1
        "dice3"
        pause 0.1
        "dice4"
        pause 0.1
        "dice5"
        pause 0.1
        "dice6"
        pause 0.1
        repeat 3
    function timer_callback
    "dice[current_dice]"

style dice_prompt_title is text:
    size 100
    xalign .5

style dice_prompt_text is text:
    size 40
    xalign .5

style dice_prompt_image is image:
    xalign .5
    yalign .5

style dice_prompt_button is image:
    xalign .5
    yalign .5

# Dice Screen definition
screen dice(title, text = ""):

    modal True

    zorder 200

    add "gui/overlay/confirm.png"

    # initialize the roll variables
    on "show" action Function(initialize_roll)


    frame:

        xalign .5
        yalign .5

        xminimum 400
        yminimum 400

        vbox:
            xalign .5
            yalign .5
            spacing 30

            label _(title):
                style "dice_prompt_title"

            if (text):
                label _(text):
                    style "dice_prompt_text"

            if is_rolling:
                image "dice_roll":
                    xalign .5
                    yalign .5
            else:
                image "dice[current_dice]":
                    xalign .5
                    yalign .5

            textbutton _("Roll ([rolls_left])"):
                # when the textbutton is clicked, the action runs
                # this blocks the action if its already rolling and if you dont have rolls left
                action If(is_rolling or rolls_left <= 0, None, [Function(roll_dice), ])
                style "dice_prompt_button"

            textbutton _("Continue"):
                # Waits for finish_rolling to jump to the next part of the code
                action If(finish_rolling,
                    Return(current_dice),
                    None)
                style "dice_prompt_button"

    key "game_menu" action None