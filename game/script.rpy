﻿define player = Character("[nameProt]",color="#dd2707")
define elly = Character("Elly")

transform half_size: 
    zoom 0.5

transform oneforth_size: 
    zoom 0.25

define flashbulb = Fade(0.2, 0.0, 0.8, color='#fff')
# The game starts here.

label start:

    # This ends the game.
    jump begining
    return
