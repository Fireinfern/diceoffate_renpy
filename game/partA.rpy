transform half_size: 
    zoom 0.5 #adjust as required
transform mid_size: 
    zoom 1.3 #adjust as required    

# Defined Characters
define Enemy1 = Character("---",color="#344276")
define Enemy2 = Character("-_--",color="#1c4e1c")

# Defined Images
image sensei angry = "images/SenseiPack_1080p_PNG/sf1_outfit1_angry.png"
image sensei angrytalk = "images/SenseiPack_1080p_PNG/sf1_outfit1_angrytalk.png"
image sensei huh = "images/SenseiPack_1080p_PNG/sf1_outfit1_huh.png"
image sensei normaltalk = "images/SenseiPack_1080p_PNG/sf1_outfit1_normaltalk.png"
image sensei pout = "images/SenseiPack_1080p_PNG/sf1_outfit1_pout.png"
image sensei normal = "images/SenseiPack_1080p_PNG/sf1_outfit1_normal.png"

# Define Backgrounds
image Canyon Flats ="images/The_Desert/Canyon Flats.png"
image Tree Shrine ="images/Wood Elves/Tree Shrine.png"
image Lab="images/underground lab/lab_undergroud_expo.png"

# Defined Variables
define failA= True
define failB= True
define failC =True

label begining: 

    play music "audio/Dark Atmosphere to Synth.ogg" volume 0.2 fadeout 0.5 fadein 0.5

    scene black
    with dissolve

    "as the shadow{cps=25} exppp ...{/cps}{cps=13}waa mmmm candy{/cps}"
    
    "hey ......      {w}" 
    with flashbulb
    extend"                up"

    
    " but the clift needs .... a just ahaaahd{w}"
    
    "Hey!!.....{w}" 
    with flashbulb
    extend" .. wake up"

    #change scene to open eyes
    #scene with a new helper

    play music "audio/The Story Continues.ogg" volume 0.2 fadeout 0.5 fadein 0.5

    scene cottage 2 with fade
    show sensei angrytalk at center, half_size:
        matrixcolor TintMatrix("#000000") * SaturationMatrix(0.0)
        #linear 2.0 matrixcolor TintMatrix("#ccccff") * SaturationMatrix(0.0)
        #linear 2.0 matrixcolor TintMatrix("#ffffff") * SaturationMatrix(1.0)
    with dissolve
    "finally, listen here I need you to get up, you are late for the presentation and... "

    "don’t give me that look of lost kid you remember why we are here right...don't tell me... "
    hide sensei angrytalk
    show sensei angry at center, half_size:
        linear 2.0 matrixcolor TintMatrix("#ffffff") * SaturationMatrix(1.0)
    with move
    "hi... soo what's going on ?"
    hide sensei angry
    show sensei angrytalk at center, half_size
    # show sensei angrytalk at center, half_size
    # with move
    "again, really ahhh we don’t have time for your amnesia working atm, ok quick run we are about to arrive at the pavilion"
    hide sensei angrytalk
    show sensei huh at center, half_size
    with move
    "and what is the show about?"
    hide sensei huh
    show sensei normaltalk at center, half_size
    with move
    "we are presenting!!! the new tech we develop for company use, hate that new stuff you work on"
    
    $ nameProt= renpy.input ("tell me, do you remember your name?")
    $ nameProt = nameProt.strip() 
    if nameProt == "":
        $ nameProt="Orlo"
        
    player "yes, I kinda start to remember and you are elen right"

    show sensei angrytalk at center, half_size
    with move
    
    elly"elen... is elly you idiot, well at least that memory damper works [nameProt]"
    hide sensei angrytalk
    show sensei normal at center, half_size
    with move
    player"memory what?"
    hide sensei normal
    show sensei pout at right, half_size
    with move

    elly"ask me later for now we need to preset this, we have 4 product the company want to show but only one of them this time."

    elly" I’ll say keep the memory stuff out, so what will you say we go with the agricultural, the military one or the communication device"
    #place character movement 
    hide sensei pout
    show sensei normal at right, half_size
    $ first_time=True
    label menuA:
        if first_time:
            menu:    
                "agriculture as in farm stuff?? well not sure why but sound ok to learn about it":
                    $ first_time=False  
                    jump choice1_farm

                "military, wait we do guns? nice ok let talk about that":
                    $ first_time=False  
                    jump choice1_guns

                "communication like a phone or something like that ":
                    $ first_time=False  
                    jump choice1_intel    
        else: 
            menu:
                "agriculture sound good ":
                    jump choice1_farm

                "military that sound better":
                    jump choice1_guns

                "so phone tingy sure":
                    jump choice1_intel          


    label choice1_farm:
        $ menu_routea = True 
        $ menu_routeB = False
        $ menu_routeC = False
        show sensei normaltalk at right, half_size
        with move
        elly "ok very well let me give you the summary on it, is a cabinet size machine that help grow some tree and bushes fruits with small spaces"
        #elly mouth close
        hide sensei normaltalk
        show sensei normal at center, half_size
        player "well that sound convenient but ..."
        menu:#oportuniti to roll 
            "maybe change the topic": 
                call screen dice("try to change the idea", "roll for persuasion")
                if _return<3:
                    $ failA=True
                    jump contin
                elif _return<5:
                    $ failA=True
                    jump menuA
                else:
                    $ failA=False
                    jump menuA       
                # Receive value in variable _return
            "did I make this ?":
                jump continu
        elly "ok then "        
        #if the roll is taken hides info on the option, if bad roll get back into this option, if good go back to menu. if 6 no penalty 

    label choice1_guns:
        $ menu_routeB = True
        $ menu_routea = False
        $ menu_routeC = False
        hide sensei normal
        show sensei normaltalk at center, half_size
        with move
        elly" ok a quick summary is... well is more of a new military equipment develop to protect and guide troops on a battlefield"
        elly" it helps in more ways but that is the short of it, is more for protecting but as you know a shield is a good weapon at times"
        #elly mouth close
        hide sensei normaltalk
        show sensei normal at center, half_size
        player" oh that is great but..."
        menu: 
            "maybe change the topic":
                call screen dice("try to change the idea", "roll for persuasion")
                if _return<3:
                    $ failB=True
                    jump contin
                elif _return<5:
                    $ failB=True
                    jump menuA
                else:
                    $ failB=False
                    jump menuA  
            "did i make this ?":
                jump continu
        #if the roll is taken hides info on the option, if bad roll get back into this option, if good go back to menu. if 6 no penalty         
    label choice1_intel:
        $ menu_routeC = True
        $ menu_routeB = False
        $ menu_routea = False
        hide sensei normal
        show sensei normaltalk at left, half_size
        with move
        elly"is a simple thing, is a new device that will improve on the communication around the world as it not only service for communication but also as a small signal amplifier"
        elly" so, the more there are around the more network there is, is not supper fancy but in can improve communication around the world"
        #elly mouth close
        hide sensei normaltalk
        show sensei normal at center, half_size
        player" fancy I think, I mean it sound simple enouph....enouf...eh altho.."
        menu: 
            "maybe change the topic":
                call screen dice("try to change the idea", "roll for persuasion")
                if _return<3:
                    $ failC=True
                    jump contin
                elif _return<5:
                    $ failC=True
                    jump menuA
                else:
                    $ failC=False
                    jump menuA  
            "did i make this ?":
                jump continu
    #if taken roll the dice, if is 1 0r 2 then no options are given and the story continues with the chose taken
    label contin:
        hide sensei normal
        show sensei angrytalk at center, half_size
        elly"we don’t have time for that"
        hide sensei angrytalk
        show sensei angry at center, half_size
        #elly should be dispointed or mad
        player" but it can be good to learn more about the other..."
        hide sensei angry
        show sensei angrytalk at center, half_size
        elly" as i told you the time is short and we are almost there so the original one it is"
        jump continu
    label continu:
        hide sensei angrytalk
        show sensei normaltalk at center, half_size
        with move
        if menu_routea:
            show sensei normaltalk at left, half_size 
            #elly to the side
            elly"ok so the idea is to grow more trees in less-than-ideal places"
        elif menu_routeB:
            #move elly to the side
            show sensei normaltalk at center, half_size
            elly"ok so the idea is protecting soldiers with a barrier like an air shield" 
        else:
            #move elly to the side
            show sensei normaltalk at right, half_size
            elly"ok so the idea is a phone that give signal to other phones"
        hide sensei normaltalk
        show sensei normal at center, half_size
        player" sound great ok what do I have to do"
        #change to talk elly 
        hide sensei normal
        show sensei normaltalk at center, half_size
        elly" well is not too much just explain to the future investor what the product is"
        #close mouth elly
        hide sensei normaltalk
        show sensei normal at center, half_size
        player" but i don’t..."
        #change to talk elly 
        hide sensei normal
        show sensei normaltalk at center, half_size
        elly" yea, yea, i know, but is you. I know you got some ideas of what it can do just by the general idea, you came up with it 4 times already so is still there"
        #move to the side and close her mouth 
        hide sensei normaltalk
        show sensei normal at left, half_size
        player" ok I trust ya "
        #talk elly
        hide sensei normal
        show sensei normaltalk at left, half_size
        elly" great now let’s run we need to be there now!!"
        hide sensei normaltalk
        #hide elly
        Enemy2"is that them?"

        Enemy1"it does look like it, lets move"

        #trantition of scene
        #elly center
        player"ok so where should we head to then"
        if menu_routea:
            jump routA
        elif menu_routeB:
            jump routB
        elif menu_routeC:
            jump routC        
    label routA:
        elly"to the indoor forest "  
        #scene change to the forest image 
        scene Tree Shrine with fade
        show sensei normaltalk at center, half_size
        show podium  behind sensei
        show podium at mid_size  
        player"why is it indoors? also why it thriving?"   
        hide sensei normal
        show sensei normaltalk at left, half_size
        elly"that be partly your fault, the dome has different environment but it waste to many resources"
        #scene change to the podium 
        hide sensei normaltalk 
        show sensei normal at left, half_size
        player"ok so what i have to do"
        hide sensei normal
        show sensei normaltalk at left, half_size
        elly"just get ready ill pass some images for the presentation and you get some of the ideas of the topic"
        hide sensei normaltalk 
        show sensei normal at left, half_size
        player"ok hope you help me too"
        hide sensei normal
        show sensei normaltalk at left, half_size
        elly"of course !!" 
        hide sensei normaltalk 
        show sensei normal at left, half_size  
        jump presentation

    label routB:
        elly"to the indoor desert exhibition center"  
        #scene change to the desert image 
        scene Canyon Flats with fade
        show sensei normaltalk at center, half_size 
        show podium  behind sensei
        show podium at mid_size    
        player"why is it indoors? also why desert I can not imagine sand being contain?"   
        hide sensei normal
        show sensei normaltalk at right, half_size
        elly"that be partly your fault, the dome has different environment but it waste to many resources"
        elly"the sand is synthetic one, mix with a bit of metal traces so it can be electrically charged to avoid damage from some test being done, altho yes is annoying"   
        #scene change to the podium 
        hide sensei normaltalk 
        show sensei normal at right, half_size 
        player"ok so what i have to do"
        hide sensei normal
        show sensei normaltalk at right, half_size
        elly"just get ready ill pass some images for the presentation and you get some of the ideas of the topic"
        hide sensei normaltalk 
        show sensei normal at right, half_size 
        player"ok hope you help me too"
        hide sensei normal
        show sensei normaltalk at right, half_size
        elly"of course !!"
        hide sensei normaltalk 
        show sensei normal at right, half_size 
        jump presentation

    label routC:
        elly"to the conference room "  
        #scene change to the forest image 
        scene Lab with fade
        show sensei normal at center, half_size
        show podium behind sensei 
        show podium at mid_size  
        player"wait there is only one, this place is huge what is all the other rooms?"   
        hide sensei normal
        show sensei normaltalk at left, half_size
        elly"oh well not much time to explain {player} but lets just say some spaces here are specially design for some projects, but I doubt we need much for the spaces so a normal conference is fine"
        #scene change to the podium 
        hide sensei normaltalk 
        show sensei normal at left, half_size
        player"ok so what I have to do"
        hide sensei normal
        show sensei normaltalk at left, half_size
        elly"just get ready ill pass some images for the presentation and you get some of the ideas of the topic"
        hide sensei normaltalk 
        show sensei normal at left, half_size
        player"ok hope you help me too"
        hide sensei normal
        show sensei normaltalk at left, half_size
        elly"of course !!"    
        hide sensei normaltalk 
        show sensei normal at left, half_size     
        jump presentation

    label presentation:
        #character aporach the podium 
        player"hello everyone"
        show sensei normal :
            linear 2.0 matrixcolor TintMatrix("#b0b0b0") * SaturationMatrix(1.0)
        player"on behave of all at ..."
        
        show sensei normal :
            linear 2.0 matrixcolor TintMatrix("#ffffff") * SaturationMatrix(1.0)   
        elly"{i}quantum leap labs{/i}"
        show sensei normal :
            linear 2.0 matrixcolor TintMatrix("#b0b0b0") * SaturationMatrix(1.0)
        
        player"quantum leap labs, we are proud to introduce our new product"
        if (menu_routea):
            player"in the spirit of making a bit of a mark in improving the enviroment and the city life"
            menu:    
                "the trees need a life to improve our life":
                    $ pointA=False  
                "our idea is to make food and environment problems a thing of the past":
                    $ pointA=True 
            show sensei normal :
                linear 2.0 matrixcolor TintMatrix("#f7f4f4") * SaturationMatrix(1.0)        
            elly"{i}keep going ^w^{/i}"
            show sensei normal :
                linear 2.0 matrixcolor TintMatrix("#b0b0b0") * SaturationMatrix(1.0)
            player"this based on our work have been a trill to work and see how much we can improve and bottle an nature"
            menu:
                "for being able to see nature thrive more help by our hands":
                    $ pointB = True
                "to reshape and control nature and make it a new":
                    $ pointB = False
            elly"yes...."
            show sensei normal :
                linear 2.0 matrixcolor TintMatrix("#f5f3f3") * SaturationMatrix(1.0)
            player"the import idea is to improve it the live of people base on this as we all know is hard to get some plant to grow in difficult terrains"
            player"for know ill like to show some of the main points of the product"
            player "is it important to know this is a first look and..."
            show sensei normal :
                linear 2.0 matrixcolor TintMatrix("#f5f3f3") * SaturationMatrix(1.0)
            elly"{i}the prototype is ready on to be show on the expo {/i}"
            show sensei normal :
                linear 2.0 matrixcolor TintMatrix("#b0b0b0") * SaturationMatrix(1.0)
            player"...will be on display so all can have a look to it"
            player"firstly we wil like to let you undertand that this device can aloud to grow any kind of plant regarless of the envirement the device is in"
            menu: 
                "it has a field that reduce the water and air needed to survive":
                    $ pointC = True
                "it clone and genetically adapt the plant to the climat of the surraunding area"   :
                    $ pointC = False 
            player"another one we mention is the capavility to reduce the not only the necesary extras for plants caring but also"
            menu:
                "it only require water every month ":
                    $ pointD =False
                "it can reduce the water needed as well as the nutrients for the soil":
                    $ pointD=True
            if (failA):
                player"lastly we have to talk about the "
                menu:
                    "reaction field that prevent most pest and also most fungal attacts":
                        $ pointD =False
                    "the intrgation withother plat and the protection form pest":
                        $ pointD=True       
            "..."            
            player"and that is why"
            menu:
                "it is a new revolution that will allow platy of people to be able to generate produce and reduce the impact on the environment":
                    $ pointF = True
                "to reshape and control nature and make it a new as the shaper for a new future":
                    $ pointF = False
            player"as you know this will be a revolution"
        #    
        if (menu_routeB):
            player"in the spirit of making a bit of a mark in improving the life of our military and police"
            menu:    
                "the new way of safety and prevention of the most danger":
                    $ pointA=True 
                "a brand new example of what we can do ":
                    $ pointA=False 
            show sensei normal :
                linear 2.0 matrixcolor TintMatrix("#f5f3f3") * SaturationMatrix(1.0)
            elly"{i}keep going ^w^{/i}"
            show sensei normal :
                linear 2.0 matrixcolor TintMatrix("#b0b0b0") * SaturationMatrix(1.0)
            player"it is on all of us to have the best interest to keep people protected and secure becuse if we keep our safety officer secure they can be"
            menu:
                "more recless and avoid being afraid of all the criminals that are out there":
                    $ pointB = True
                "more relax and aproach in a new way the situations there trown in":
                    $ pointB = False
            show sensei normal :
                linear 2.0 matrixcolor TintMatrix("#f5f3f3") * SaturationMatrix(1.0)
            elly"yes...."
            show sensei normal :
                linear 2.0 matrixcolor TintMatrix("#b0b0b0") * SaturationMatrix(1.0)
            player"the import idea is to improve all our lives and that start with the first line of defence we have, so this product will be a new safety net"
            player"for know ill like to show some of the main points of the product"
            player "is it important to know this is a first look and..."
            show sensei normal :
                linear 2.0 matrixcolor TintMatrix("#f5f3f3") * SaturationMatrix(1.0)
            elly"{i}the prototype is ready on to be show on the expo {/i}"
            show sensei normal :
                linear 2.0 matrixcolor TintMatrix("#b0b0b0") * SaturationMatrix(1.0)
            player"...will be on display so all can have a look to it"
            #STOPT HERE
            player"firstly we wil like to let you undertand that this is a protection device it help in"
            menu: 
                "creating a air bubble that will defend agains many projectile attacks":
                    $ pointC = True
                "creating a repelent with a force field tha vaporice any treaths"   :
                    $ pointC = False 
            player"another one we mention is the capavility to reduce the affect of the enviroment "
            menu:
                "it will reduce the heat and cold that affect the user":
                    $ pointD =False
                "as it removes the gases and enviromental damages that can affect the user":
                    $ pointD=True
            if (failB):
                player"lastly we have to talk about the attact funtion "
                menu:
                    "the fuild can use the kinetic energy to attack potencial treaths to the user and to others":
                        $ pointD =False
                    "it has a mini bubble launcher that project a small shield, punching things away and puting harmfull element in a close enviroment":
                        $ pointD=True       
            "..."            
            player"and that is why"
            menu:
                "it is a new revolution that will allow plenty of people to keep all safe and our arm forces to trive":
                    $ pointF = True
                "to reshape and control people, for there and our safety ":
                    $ pointF = False
            player"as you know this will be a revolution" 

        else:
            player"in the spirit of making a bit of a mark in improving the life of people everywhere"
            menu:    
                "a new telecomunication addon ":
                    $ pointA=True 
                "a brand new sistem on telecomunication ":
                    $ pointA=False 
            show sensei normal :
                linear 2.0 matrixcolor TintMatrix("#f5f3f3") * SaturationMatrix(1.0)
            elly"{i}keep going ^w^{/i}"
            show sensei normal :
                linear 2.0 matrixcolor TintMatrix("#b0b0b0") * SaturationMatrix(1.0)
            player"it is on all of us to have the best interest to keep people connected and have the fast and avaliable network for that to happen"
            menu:
                "that is why this phone can conect to all satelites":
                    $ pointB = True
                "that is why this phone can become an antena that help the signal increase and connnect to other devices":
                    $ pointB = False
            show sensei normal :
                linear 2.0 matrixcolor TintMatrix("#f5f3f3") * SaturationMatrix(1.0)
            elly"yes...."
            show sensei normal :
                linear 2.0 matrixcolor TintMatrix("#b0b0b0") * SaturationMatrix(1.0)
            player"the import idea is to improve all our lives and that start with the reaching all so we can really have a telecomunication network"
            player"for know ill like to show some of the main points of the product"
            player "is it important to know this is a first look and..."
            show sensei normal :
                linear 2.0 matrixcolor TintMatrix("#f5f3f3") * SaturationMatrix(1.0)
            elly"{i}the prototype is ready on to be show on the expo {/i}"
            show sensei normal :
                linear 2.0 matrixcolor TintMatrix("#b0b0b0") * SaturationMatrix(1.0)
            player"...will be on display so all can have a look to it"
            player"firstly we wil like to let you undertand that "
            menu: 
                "it is a normal phone as many has come before":
                    $ pointC = True
                "it more than a communication divice "   :
                    $ pointC = False 
            player"it can help connecting at faster speed around the world alowing communication and data tranfer to change"
            menu:
                "it only require a small charge to work ":
                    $ pointD =False
                "it has a more that good batery that can work for days constantly if not weeks ":
                    $ pointD=True
            if (failA):
                player"lastly we have to talk about the "
                menu:
                    "the interconection aloud for faster tranfer data speeds":
                        $ pointD =False
                    "it come with an integrated Ai that can help you make your life easier":
                        $ pointD=True       
            "..."            
            player"and that is why"
            player"as you know this will be a revolution" 

        Enemy1"our revolution....."
        #scene change as an attack happen 
        jump mid_attack
        


                    



